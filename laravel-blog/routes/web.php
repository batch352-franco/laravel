<?php

use Illuminate\Support\Facades\Route;
// link the Post Controller Class
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// define a route wherein to create a post will be returned to the user

Route::get('/post/create', [PostController::class, 'create'] );

// define a route wherein form data will be sent via POST method to the /post URI endpoint.
Route::post('/posts', [PostController::class, 'store']);

// define a route that will return a view containing all posts
Route::get('/posts', [PostController::class, 'index']);

// so2 activity
// define a route that will return a view for the welcome page
Route::get('/', [PostController::class, 'welcome']);