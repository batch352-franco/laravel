{{-- s02 activity solution --}}

@extends('layouts.app')

@section('content')

    <div class="containter">
        <div class="row d-flex justify-content-center p-4">
            <img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" class="img-fluid col-lg-6">
        </div>
        <div class="row text-center p-3">
            <h3>Featured Posts:</h3>
        </div>
    </div>

    @if(count($posts) > 0)
        @foreach($posts as $post)
            <div class="card text-center my-3 mx-5">
                <div class="card-body">
                    <h4 class="card-title mb-3">
                        <a href="/posts/{{$post->id}}"> {{$post->title}} </a>
                    </h4>
                    <h6 class="card-title mb-3">
                        Author: {{$post->user->name}}
                    </h6>
                </div>
            </div>
        @endforeach
    @else()
        <div class="card text-center my-3 mx-5">
            <h2>There are no posts to show</h2>
            <a href="/posts/create" class="btn btn-info">Create Post</a>
        </div>
    @endif()
@endsection()