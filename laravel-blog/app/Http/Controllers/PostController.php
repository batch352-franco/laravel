<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// access to the authenticated user via the auth facade
use Illuminate\Support\Facades\Auth;

// implement the database manipulation
use App\Models\Post;

class PostController extends Controller
{
    // action to return a view containing a form for a blog post creation.
    public function create()
    {
        return view('posts.create');
    }

    // action to receive form data and subsequently store said data in the posts table.
    // Laravel's "Request" class contains the data from the form submission
    public function store(Request $request)
    {
        // if there is an authenticated user.
        if(Auth::user()){
            // Instantiate a new Post object from the Post model class.
            $post = new Post;
            // define the properties of the $post object using the received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            // get the id of the authenticated user and set it as the foreign key value of user_id of the new post.
            $post->user_id = (Auth::user()->id);

            // save this ppost object in the database
            $post->save();

            return redirect('/posts');
        } else {
            return redirect('/login');
        }
    }

    // action that will return a view showing all the blog posts
    public function index()
    {
        // Fetches all records from the table without any specific condition.
        // $posts = Post::all();

        // Fetches all records from the table with specific conditions. (if need to build more complex queries)
        $posts = Post::get();

        // with() method is used to pass data to views file.
        // thiss assigns a variable name ('posts') and its corresponding data ($posts collection)
        return view('posts.index')->with('posts', $posts);
    }

    // s02 activity
    // action that will return a view showing 3 random blogpost

    public function welcome()
    {

        $posts = Post::get()->shuffle();

        if (count($posts) > 3) {
            $posts = $posts->random(3);
        }

        return view('welcome')->with('posts', $posts);
    }
}
